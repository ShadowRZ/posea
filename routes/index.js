'use strict'

const express = require('express')

var router = new express.Router()

router.get('/', (req, res) => {
  res.json({
    ok: true
  })
})

router.use('/api', require('./api'))

module.exports = router
