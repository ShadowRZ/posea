'use strict'

const path = require('path')
const express = require('express')
const cookieParser = require('cookie-parser')
const logger = require('./lib/logger')
const settings = require('./lib/settings')

var app = express()

// Apply settings
for (let i in settings) {
  app.set(i, settings[i])
}

// Log requests
app.use((req, res, next) => {
  logger.verbose(`New request: ${req.method} ${req.url}`)
  next()
})

// Load middlewares
app.use(cookieParser())

// Install routes within app root
app.use(settings['app root'], express.static(path.join(__dirname, 'public')))
app.use(settings['app root'], require('./routes'))

// Return 404 if no route matched
app.use(function(req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// Output error as JSON
app.use(function(err, req, res, next) {
  res.status(err.status || 500)
  res.json({
    error: {
      status: err.status || 500,
      message: err.message
    }
  })
})

module.exports = app
