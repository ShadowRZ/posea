# Posea

A self-hosted embedded comment system for websites (espacially static blogs).

Currently in development, for more information, read [this article (Chinsese)](https://fiveyellowmice.com/posts/2017/02/no-more-delays-i-going-make-comment-system.html).


## Copyright

Posea
Copyright (C) 2017 FiveYellowMice

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
