'use strict'

const Sequelize = require('sequelize')
const path = require('path')
const settings = require('./settings')

var db = new Sequelize({
  dialect: 'sqlite',
  storage: path.join(__dirname, '..', 'data', settings['env'] + '.db')
})

var Post = db.define('post', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  title: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  url: {
    type: Sequelize.TEXT,
    allowNull: false,
    unique: true
  }
}, {
  timestamps: false
})

var User = db.define('user', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  username: {
    type: Sequelize.TEXT,
    allowNull: false,
    unique: true
  },
  nickname: {
    type: Sequelize.TEXT,
    allowNull: true,
    unique: true
  },
  email: {
    type: Sequelize.TEXT,
    allowNull: true,
    unique: true
  }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
})

var Comment = db.define('comment', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  text: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  deleted: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false
  }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
})

Post.hasMany(Comment, {
  as: 'Comment',
  foreignKey: {
    field: 'posted_on',
    allowNull: false
  }
})
Comment.belongsTo(Post, {
  as: 'PostedOn',
  foreignKey: {
    field: 'posted_on',
    allowNull: false
  }
})

User.hasMany(Comment, {
  as: 'Comment',
  foreignKey: {
    field: 'author',
    allowNull: false
  }
})
Comment.belongsTo(User, {
  as: 'Author',
  foreignKey: {
    field: 'author',
    allowNull: false
  }
})

Comment.hasMany(Comment, {
  as: 'Replies',
  foreignKey: {
    field: 'replies_to',
    allowNull: true
  }
})
Comment.belongsTo(Comment, {
  as: 'RepliesTo',
  foreignKey: {
    field: 'replies_to',
    allowNull: true
  }
})

db.sync()

module.exports = {
  db: db,
  Post: Post,
  User: User,
  Comment: Comment
}
